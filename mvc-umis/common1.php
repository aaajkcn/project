<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<head>
 <link href="assets/css/bootstrap.min.css" rel="stylesheet">

 <!-- Custom styles for this template -->
 <link href="assets/css/offcanvas.css" rel="stylesheet">
 <link href="assets/css/style.css" rel="stylesheet">
 <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>
    <style>

      #nav {
        line-height:30px;
        background-color:#eeeeee;
        height:540px;
        width:200px;
        float:left;
        padding:5px;        
      }
      #section {
        width:350px;
        float:left;
        padding:10px;    
      }
      #footer {
        background-color:pink;
        color:blue;
        clear:both;
        text-align:center;
        padding:5px;    
      }
    </style>
  </head>

  <body>

    <div class="container-fluid">
      <!--nav开始-->
      <ul class="nav nav-tabs">
        <li role="presentation" class="active"><a href="index.php?c=index&m=index">首页</a></li>
        <li role="presentation"><a href="index.php?c=index&m=listall&tid=1">男装</a></li>
        <li role="presentation"><a href="index.php?c=index&m=listall&tid=2">女装</a></li>
        <li role="presentation"><a href="index.php?c=index&m=listall&tid=3">数码</a></li>
        <li role="presentation"><a href="index.php?c=index&m=listall&tid=4">运动</a></li>
        <li role="presentation"><a href="index.php?c=index&m=listall&tid=5">生活</a></li>
        <li role="presentation"><a href="index.php?c=index&m=listall&tid=6">其它</a></li>
        <form
        action="search.php"class="navbar-form navbar-left" role="search" >
        <div class="form-group">
          <input
          placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">搜索</button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li class="active"><a href="">欢迎<?php
          session_start();
          if(isset($_SESSION["uname"]))
            echo " ".$_SESSION["uname"]." ";
          else{
            echo "您";
          }
          ?>访问本网站</a>
        </li>
        <?php
//登录成功才显示
        if(isset($_SESSION["uname"]))
        {
          ?>
          <li><a href="index.php?c=index&m=logout">注销</a></li>
          <li><a href="index.php?c=index&m=addgoods&uname=<?php echo $_SESSION['uname'];?>">个人中心</a></li>
          <?php
        }else{
          ?>

          <li role="presentation"><a href="index.php?c=index&m=login">登录</a></li>
          <li role="presentation"><a href="index.php?c=index&m=reguser">注册</a></li>
          <?php
        }
        ?>
      </ul>
    </div>



    <div id="nav" >
<table align="center" class="table table-bordered">
<tr>
  <td class="text-center"><a href="index.php?c=index&m=login">我的消息</a></td>
</tr>
<tr>
  <td class="text-center"><a href="index.php?c=index&m=reguser">我的购物车</a></td>
</tr>
<tr>
  <td class="text-center"><a href="index.php?c=index&m=addgoods&uname=<?php echo $_SESSION['uname'];?>">发布宝贝</a></td>
</tr>
<tr>
  <td class="text-center"><a href="index.php?c=index&m=good0&uname=<?php echo $_SESSION['uname'];?>&sign=0">我发布的宝贝</a></td>
</tr>
<tr>
  <td class="text-center"><a href="index.php?c=index&m=good1&uname=<?php echo $_SESSION['uname'];?>&sign=1">已买到的宝贝</a>
  </td>
</tr>
<tr>
  <td class="text-center"><a href="index.php?c=index&m=moduser&uname=<?php echo $_SESSION['uname'];?>">修改个人资料</a></td>
</tr>
<tr>
  <td class="text-center"><a href="index.php?c=index&m=changepassword&uname=<?php echo $_SESSION['uname'];?>">修改密码</a></td>
</tr>
<tr>
  <td class="text-center"><a href="index.php?c=index&m=reguser">联系客服</a>
  </td>
</tr>
</table>
</div>

<div id="section">
 

