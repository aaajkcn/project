<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>商品信息</title>
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
</head>
<body>
     <h1 align="center" style="margin-right:20px">商品信息列表</h1>
    <form method="post" action="" class="col-md-4 control-label col-md-offset-5">
      请输入商品名:
      <input type="text" name="key">
      <input type="submit" name='search' value="搜索" class="btn btn-success">
    </form>

     <table align="center" class="table table-bordered">    
<tr>
  <th class="text-center">编号</th>
  <th class="text-center">商家名</th>
  <th class="text-center">商品名</th>
  <th class="text-center">类别</th>
  <th class="text-center">电话</th>
  <th class="text-center">图片</th>
  <th class="text-center">介绍</th>
  <th class="text-center">价格</th>
  
  <th class="text-center">操作  <a href="admin.php?c=good&m=form">添加</a></th>
  </tr>

    <?php
      $i=1;
     	foreach($data['Goods'] as $row){
			echo'<tr>';
			echo '<td class="text-center">'.$i++.'</td>';
			echo'<td class="text-center">'.$row['uname'].'</td>';
      echo'<td class="text-center">'.$row['goodname'].'</td>';
			switch($row['tid']){
				case'1':$tp="男装"; break;
				case'2':$tp="女装"; break;
        case'3':$tp="数码"; break;
				case'4':$tp="运动"; break;
        case'5':$tp="生活"; break;
        case'6':$tp="其他"; break;
				default:$tp="不知道";
			}
			echo '<td class="text-center">'.$tp.'</td>';
  echo'<td class="text-center">'.$row['phone'].'</td>';

			echo '<td align="center"><img src='."upload/".$row['pic'].' width=60 height=80 alt="海报" class="img-circle"></td>';
			echo'<td class="text-center">'.$row['intro'].'</td>';
			echo'<td class="text-center">'.$row['price'].'</td>';
			echo '<td class="text-center"><a href="admin.php?c=good&m=mod&id='.$row['gid'].'" class="btn btn-success" href="#" role="button">修改</a>|<a href="admin.php?c=good&m=del&id='.$row['gid'].'" onClick="return confirm(\'确定要删除吗？\');" class="btn btn-danger" href="#" role="button">删除</a></td>';
            echo '</tr>';

        }
        echo '</table>';
    if ($rowsperpage>=$totalrows) 
        $totalpages=1;
     else{
       $totalpages=ceil($totalrows/$rowsperpage);
    }
    //如果不是第一页，则显示第一页和上一页的超链接，否则只显示文字
    //echo '<nav><u1 class="pager">';
    
    //输出转到几页的表单
    ?>

</body>
</html>