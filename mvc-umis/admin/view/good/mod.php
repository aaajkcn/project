<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title></title>
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
</head>
<body>

<?php
   $row = $data['goods'];
   // var_dump($row); 
     $id=$_GET['id']; 
   // var_dump($id);
   $phone=$_GET['phone'];
   // var_dump($phone);
   $uname = $row['uname'];
 $phone=$row['phone'];
$tid=$row['tid'];

$intro=$row['intro'];
$price=$row['price'];
 
   $pic = 'upload/'.$row['pic'];    
  //将查询结果显示在表单中
 ?>
 <form method="post"  action="admin.php?c=good&m=update"  enctype="multipart/form-data" class="form-horizontal" >

<h1 align="center" style="margin-right:20px">修改商品信息</h1>

<div class="container-fluid col-md-12">  
      <input type="hidden" name="id" value=<?php echo $id; ?>>

 <div class="form-group">
        <label for="exampleInputUserName1" class="col-md-1 control-label col-md-offset-4">商家名字</label>
        <div class="col-md-3">
          <input type="text" name="uname" class="form-control"  value=<?php  echo $uname;  ?>>
        </div>
      </div>
      <div class="form-group">
        <label for="exampleInputUserName1" class="col-md-1 control-label col-md-offset-4">商品名称</label>
        <div class="col-md-3">
          <input type="text" name="goodname" class="form-control"  value=<?php  echo $goodname;  ?>>
        </div>
      </div>

      <div class="form-group">
        <label for="exampleInputAge1" class="col-md-1 control-label col-md-offset-4">价格</label>
        <div class="col-md-3">
          <input type="number" name="price" class="form-control"  value=<?php  echo $data['gid']['price'];  ?>>
        </div>
      </div>
       <div class="form-group">
        <label for="exampleInputAge1" class="col-md-1 control-label col-md-offset-4">手机号码</label>
        <div class="col-md-3">
           <input type="int" name="phone" class="form-control"  value=<?php  echo $data['gid']['phone'];  ?>>
        </div>
      </div>


      <div class="form-group">
        <label class="col-md-1 control-label col-md-offset-4">商品类型</label>
        <div class="col-md-3">
          <select class="form-control" name="tid">
            <option value="">请选择</option>
            <option value=1 <?php  if($data['gid']['tid']=='1') echo 'selected';  ?>>男装</option>
            <option value=2 <?php  if($data['gid']['tid']=='2') echo 'selected';  ?>>女装</option>
            <option value=3 <?php  if($data['gid']['tid']=='3') echo 'selected';  ?>>数码</option>
            <option value=4 <?php  if($data['gid']['tid']=='4') echo 'selected';  ?>>运动</option>
            <option value=5 <?php  if($data['gid']['tid']=='5') echo 'selected';  ?>>生活</option>
            <option value=6 <?php  if($data['gid']['tid']=='6') echo 'selected';  ?>>其它</option>
          </select>
        </div>
      </div>



      <div class="form-group">
        <label for="exampleInputFile" class="col-md-1 control-label col-md-offset-4">商品图片</label>
        <div class="col-md-3">
          <img src='<?php  echo "photos".$data['gid']['pic'];  ?>' width=100 height=150>
         <input type="file" name="pic">
        </div>
      </div>

           <div class="form-group">
        <label for="exampleInputTextarea1" class="col-md-1 control-label col-md-offset-4">商品介绍</label>
        <div class="col-md-3">
          <textarea name="intro" cols="40" rows="2"><?php  echo $data['gid']['intro'];?></textarea>
        </div>
      </div>
       <div class="form-group">
        <div class="col-sm-offset-5 col-sm-5">
          <input type="submit" class="btn btn-default" value="发布">
          <input type="reset" class="btn btn-default" value="重置">
        </div>
      </div>

</form>
</div>



</body>
</html>