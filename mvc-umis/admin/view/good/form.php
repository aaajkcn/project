<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title></title>
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
</head>
<body>
<h1 align="center" style="margin-right:20px">添加商品信息</h1>
    <form method=post action="admin.php?c=good&m=add" enctype="multipart/form-data" class="form-horizontal">
      <div class="form-group">
        <label for="exampleInputUserName1" class="col-md-5 control-label">商品名称</label>
        <div class="col-md-3">
          <input type="text" name="goodname" class="form-control" id="exampleInputUserName1" placeholder="名称" required>
        </div> </div>

<div class="form-group">
  <label for="exampleInputUserName1" class="col-md-5 control-label">卖家姓名</label>
        <div class="col-md-3">
          <input type="text" name="uname" class="form-control" id="exampleInputUserName1" placeholder="卖家姓名" required>
        </div> </div>

    <div class="form-group">
                <label  for="inputEmail3" class="col-sm-5 control-label">类别</label>
                <div class="col-sm-4">
                    <select name="tid" class="form-control">
                        <option value="1">男装</option>
                        <option value="2">女装</option>
                        <option value="3">数码</option>
                        <option value="4">运动</option>
                        <option value="5">生活</option>
                        <option value="6">其它</option>
                    </select>
                </div>
            </div>


  <div class="form-group">
        <label for="exampleInputFile" class="col-md-5 control-label">商品图片</label>
        <div class="col-md-3">
          <input type="file" name="pic" id="exampleInputFile" required>
        </div>
      </div>
 <div class="form-group">
        <label for="exampleInputTextarea1" class="col-md-5 control-label">商品介绍</label>
        <div class="col-md-3">
          <textarea class="form-control" rows="3" id="exampleInputTextarea1" placeholder="商品介绍" name="intro"></textarea>
        </div>
      </div>

<div class="form-group">
        <label for="exampleInputTextarea1" class="col-md-5 control-label">商家电话</label>
        <div class="col-md-3">
          <textarea class="form-control" rows="3" id="exampleInputTextarea1" placeholder="商家电话" name="phone"></textarea>
        </div>
      </div>

  <div class="form-group">
        <label for="exampleInputAge1" class="col-md-5 control-label">价格</label>
        <div class="col-md-3">
          <input  name="price" class="form-control" id="exampleInputPassword1" placeholder="商品价格" required>
        </div>
      </div>





      <div class="form-group">
        <div class="col-sm-offset-5 col-sm-10">
          <input type="submit" class="btn btn-success" value="提交">
          <input type="reset" class="btn btn-primary" value="重置">
        </div>
      </div>
    </form>

</body>
</html>