<?php
class ViewModel
{
	private $data=array();
	private $rander=false;
	public function __construct($template){
		$file='admin/view/'.strtolower($template).".php";
		if (file_exists($file)) {
			$this->rander=$file;
		}
	}
	public function assign($variable,$value){
		$this->data[$variable]=$value;
	}
	public function display(){
		$data=$this->data;
		include($this->rander);
	}
}
?>