<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>用户管理系统</title>
</head>
<body>
<?php
require "admin/model/UserModel.php";
require "admin/controller/Controller.php";
class UserController extends Controller
{
	function __construct()
	{
		parent::__construct();
		if(!isset($this->model)){
			$this->model=new UserModel;
		}
		$this->mod='user';
	}
	function table()
	{
		// $model=new UserModel();
		$data=$this->model->getTable();
		// var_dump($data);
		$this->assign('user',$data);
		$this->display();
		// return $data;
	}
	function form(){
		$this->display();
	}
	function add(){
		$data = $_POST;
		// $model = new UserModel();
		$result = $this->model->add($data);
		if($result){
			echo '添加成功 <a href="admin.php?c=user&m=table">如果没有跳转，请点这里跳转</a>';
   			header("refresh:3;url='admin.php?c=user&m=table'");
		}
		else{
			echo "添加失败！<br/>";
  			echo "<a href='admin.php?c=user&m=form'>返回</a>";
		}
	}
	function mod(){
		$id = $_GET['id'];
		// var_dump($id) ;
		// $model = new UserModel();
		$result = $this->model->getById($id);
		$this->assign('user',$result);
		$this->display(); 
	}

	function update(){
		$data = $_POST;
		// $model = new UserModel();
		$result = $this->model->update($data);
		if($result){
			echo '更新成功 <a href="admin.php?c=user&m=table">如果没有跳转，请点这里跳转</a>';
   			header("refresh:3;url='admin.php?c=user&m=table'");
		}
		else{
			echo "更新失败！<br/>";
  			echo "<a href='admin.php?c=user&m=mod&id=".$data['id']."'>返回</a>";
		}
	}
	function del(){
		$id = $_GET['id'];
		// $model = new UserModel();
		$result = $this->model->del($id);
		if($result){
			echo '删除成功 <a href="admin.php?c=user&m=table">如果没有跳转，请点这里跳转</a>';
   			header("refresh:3;url='admin.php?c=user&m=table'");
		}
		else{
			echo "删除失败！<br/>";
  			echo "<a href='admin.php?c=user&m=table'>返回</a>";
		}
	}
}
?>
</body>
</html>