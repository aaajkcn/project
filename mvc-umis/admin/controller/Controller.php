<?php
require "admin/ViewModel.php";
class Controller
{
	protected $view;
	function __construct()
	{
		$this->view=new ViewModel($_GET['c'].'/'.$_GET['m']);
	}
	function assign($variable,$value){
		$this->view->assign($variable,$value);
	}
	function display(){
		$this->view->display();
	}
	
}
?>