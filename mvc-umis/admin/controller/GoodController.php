<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
</head>
<body>
<?php
require "admin/model/GoodModel.php";
require "admin/controller/Controller.php";
class GoodController extends Controller
{
	function __construct()
	{
		parent::__construct();
		$this->model=new GoodModel;
	}
	function table()
	{
		// $model=new VideoModel();
		$data=$this->model->getTable();
		$this->assign('Goods',$data);
		$this->display();
		// return $data;
	}
	function form(){
		$this->display();
	}
	function add(){
		$data = $_POST;
		// $model = new VideoModel();
		$result = $this->model->add($data);
		if($result){
			echo '添加成功 <a href="admin.php?c=Good&m=table">如果没有跳转，请点这里跳转</a>';
   			header("refresh:3;url='admin.php?c=Good&m=table'");
		}
		else{
			echo "添加失败！<br/>";
  			echo "<a href='admin.php?c=Good&m=form'>返回</a>";
		}
	}
	function mod(){
		$id = $_GET['id'];
		// var_dump($id) ;
		// $model = new VideoModel();
		$result = $this->model->getById($id);
		$this->assign('Goods',$result);
		$this->display(); 
	}

	function update(){
		$data = $_POST;
		// $model = new VideoModel();
		$result = $this->model->update($data);
		if($result){
			echo '更新成功 <a href="admin.php?c=Good&m=table">如果没有跳转，请点这里跳转</a>';
   			header("refresh:3;url='admin.php?c=Good&m=table'");
		}
		else{
			echo "更新失败！<br/>";
  			echo "<a href='admin.php?c=Good&m=mod&id=".$data['id']."'>返回</a>";
		}
	}
	function del(){
		$id = $_GET['id'];
		// $model = new GoodModel();
		$result = $this->model->del($id);
		if($result){
			echo '删除成功 <a href="admin.php?c=Good&m=table">如果没有跳转，请点这里跳转</a>';
   			header("refresh:3;url='admin.php?c=Good&m=table'");
		}
		else{
			echo "删除失败！<br/>";
  			echo "<a href='admin.php?c=Good&m=table'>返回</a>";
		}
	}
}
?>
</body>
</html>