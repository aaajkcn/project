<?php
require "admin/model/Model.php";
class UserModel extends Model
{
	function __construct()
	{
		parent::__construct();
		$this->mod='users';
	}
	function getTable()
	{
		$sql="select * from {$this->mod}";
		$data=$this->conn->query($sql);
		// var_dump($data);
		return $data;
	}
	function add($data)
	{
		if(!empty($_FILES["pic"]["name"])){
  				$pic = $this->uploadfile('pic','./upload/',array('jpg','gif','png'));
  				}
			  else{
			  	$pic='';
			  }

			  $user = array(
					'name' => trim($data['sername']),  
					'password' => $data['password'],
					'gender' => $data['gender'],
					'birthdate' => $data['birthdate'],
					
					'adress' => $data['adress'],
					
					'pic'=>$pic
			  	);
			  $keys = implode(',',array_keys($user));
			  $values = implode("','", array_values($user));
			  $sql="insert into {$this->mod} ({$keys}) VALUES ('{$values}')";
			  //执行SQL语句
			  $result = $this->conn->query($sql);
			  return $result;
	}
	function getById($id)
	{
		$sql="select * from {$this->mod} where uid=".$id;
		$result=$this->conn->query($sql);
		return $result;	
	}
	function update($data){
			$user = array(
					'name' => trim($data['sername']),  
					//'password' => $data['password'],
					'gender' => $data['gender'],
					'birthdate' => $data['birthdate'],
					
					'adress' => $data['adress'],
					
			  	);
			if(!empty($_FILES["pic"]["name"])){
  				$pic = $this->uploadfile('pic','./upload/',array('jpg','gif','png'));
  				$user['pic'] = $pic;
  				}	 		  
			$sets = '';
			foreach ($user as $key=>$val){
			        $sets.=$key."='".$val."',";
			    }
			    $sets=rtrim($sets,','); //去掉SQL里的最后一个逗号
			   
			    $sql="UPDATE {$this->mod} SET {$sets} where uid=".$data['id'];
			    //执行SQL语句
				$result = $this->conn->query($sql);
				return $result;
	}
	function del($id){
			$sql = "delete from {$this->mod} where uid=".$id;
			$result = $this->conn->query($sql);
			return $result;
	}
}
?>