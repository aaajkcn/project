/*
MySQL Data Transfer
Source Host: localhost
Source Database: market
Target Host: localhost
Target Database: market
Date: 2016/9/8 17:47:07
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for admin
-- ----------------------------
CREATE TABLE `admin` (
  `aid` int(20) NOT NULL AUTO_INCREMENT,
  `aname` varchar(20) NOT NULL,
  `apass` varchar(20) NOT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for goods
-- ----------------------------
CREATE TABLE `goods` (
  `gid` int(20) NOT NULL AUTO_INCREMENT,
  `uname` varchar(20) DEFAULT NULL,
  `goodname` varchar(20) NOT NULL,
  `tid` int(11) unsigned NOT NULL DEFAULT '6',
  `sign` int(5) unsigned zerofill NOT NULL DEFAULT '00000' COMMENT '0代表我发布的宝贝    1代表已买到的宝贝',
  `price` varchar(20) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `pic` varchar(255) DEFAULT NULL,
  `intro` text,
  PRIMARY KEY (`gid`),
  KEY `tid` (`tid`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for goodtype
-- ----------------------------
CREATE TABLE `goodtype` (
  `tid` int(11) NOT NULL AUTO_INCREMENT,
  `typename` varchar(20) NOT NULL,
  PRIMARY KEY (`tid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for users
-- ----------------------------
CREATE TABLE `users` (
  `uid` int(20) NOT NULL AUTO_INCREMENT,
  `uname` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `gender` tinyint(4) NOT NULL,
  `birthdate` date NOT NULL,
  `pic` varchar(255) NOT NULL,
  `adress` varchar(50) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'li', '123');
INSERT INTO `goods` VALUES ('4', '123', 'T恤', '1', '00001', '523', '12321313207', null, '好看');
INSERT INTO `goods` VALUES ('7', '用户甲', '上衣', '2', '00000', '45', '13567563326', null, '发个短信参与');
INSERT INTO `goods` VALUES ('8', '用户甲', '手机', '3', '00001', '5200', '12356656599', null, '阿斯顿发我');
INSERT INTO `goods` VALUES ('9', '用户甲', 'asrf24', '3', '00000', '23423', '324235235345', '20160907094312790.jpg', 'dfsgdfg');
INSERT INTO `goods` VALUES ('10', '用户甲', 'afd', '5', '00000', '2345', '2342543634', '20160907101923783.jpg', '放过机会');
INSERT INTO `goods` VALUES ('11', null, '', '0', '00000', '', null, null, null);
INSERT INTO `goods` VALUES ('12', '用户甲', '液晶显示器', '3', '00000', '897', '13434544543', '20160907233854461.png', '显示器');
INSERT INTO `goods` VALUES ('13', '用户甲', '连衣裙', '2', '00000', '98', '13243534545', '20160907234026832.png', '连衣裙');
INSERT INTO `goods` VALUES ('14', '用户甲', '马克杯', '5', '00000', '13', '24234232354', '20160907234119866.png', '杯子');
INSERT INTO `goods` VALUES ('15', '用户甲', '摩托车头盔', '4', '00001', '80', '13243534545', '20160907234203230.png', '头盔');
INSERT INTO `goods` VALUES ('16', '用户甲', '木质电脑桌', '6', '00000', '128', '13434544543', '20160907234311548.png', '电脑桌');
INSERT INTO `goods` VALUES ('17', '用户甲', '尼康D700', '3', '00000', '5433', '13243534545', '20160907234353672.png', '摄像机');
INSERT INTO `goods` VALUES ('18', '用户甲', '华为手机', '3', '00001', '2344', '13243534545', '20160907234451308.png', '手机');
INSERT INTO `goods` VALUES ('19', '用户甲', 'T恤', '1', '00000', '34', '13243534545', '20160907234545744.png', '上衣');
INSERT INTO `goods` VALUES ('20', '用户甲', '运动服', '1', '00000', '98', '13243534545', '20160907234626916.png', '运动服');
INSERT INTO `goods` VALUES ('21', '用户甲', '飞利浦电吹风', '3', '00000', '90', '13243534545', '20160907234734999.png', '电吹风');
INSERT INTO `goods` VALUES ('22', '用户甲', '柒牌衬衫', '2', '00001', '78', '13243534545', '20160907234830350.png', '衬衫');
INSERT INTO `goods` VALUES ('23', '用户甲', '条纹连衣裙', '2', '00000', '96', '13243534545', '20160907234921267.png', '连衣裙');
INSERT INTO `goods` VALUES ('24', '用户甲', '尤尼克斯羽毛球', '4', '00000', '325', '13243534545', '20160907235009812.png', '羽毛球');
INSERT INTO `goods` VALUES ('25', '用户甲', '榨汁机', '6', '00000', '126', '13243534545', '20160907235042382.png', '榨汁机');
INSERT INTO `goods` VALUES ('26', '用户甲', '裙子', '2', '00000', '45', '13243534545', '20160907235523203.png', '裙子');
INSERT INTO `goods` VALUES ('27', '用户甲', '女士皮鞋', '2', '00000', '567', '13243534545', '20160908075918826.jpg', '引领时尚');
INSERT INTO `goods` VALUES ('28', '用户甲', '西服', '1', '00000', '456', '13243534545', '20160908080010172.jpg', '的复古花朵');
INSERT INTO `goods` VALUES ('29', '用户甲', '毛衣', '2', '00000', '134', '13243534545', '20160908080049277.jpg', '岁的法国');
INSERT INTO `goods` VALUES ('30', '用户甲', '登山鞋', '1', '00000', '579', '13243534545', '20160908080128809.jpg', '大使馆');
INSERT INTO `goods` VALUES ('31', '用户甲', '眼睛', '6', '00000', '765', '13243534545', '20160908080218396.jpg', '的风格和');
INSERT INTO `goods` VALUES ('32', '用户甲', '短袖', '1', '00000', '57', '13243534545', '20160908080322394.jpg', '多元化');
INSERT INTO `goods` VALUES ('33', '用户甲', '手表', '3', '00000', '368', '13243534545', '20160908080405742.jpg', '的风景法国皇家空军');
INSERT INTO `goods` VALUES ('34', '用户甲', '手表', '3', '00000', '354', '13243534545', '20160908080508630.jpg', '的复古花朵');
INSERT INTO `goods` VALUES ('35', '用户甲', '太阳眼镜', '6', '00000', '684', '13243534545', '20160908080556639.jpg', '法国警方');
INSERT INTO `goods` VALUES ('36', '用户甲', '短袖', '1', '00000', '234', '13243534545', '20160908080636152.jpg', '的环境规划');
INSERT INTO `goods` VALUES ('37', '用户甲', '休闲西服', '1', '00000', '435', '13243534545', '20160908080715907.jpg', '东方红');
INSERT INTO `goodtype` VALUES ('1', '男装');
INSERT INTO `goodtype` VALUES ('2', '女装');
INSERT INTO `goodtype` VALUES ('3', '数码');
INSERT INTO `goodtype` VALUES ('4', '运动');
INSERT INTO `goodtype` VALUES ('5', '生活');
INSERT INTO `goodtype` VALUES ('6', '其他');
INSERT INTO `users` VALUES ('1', '用户甲', '1234', '0', '0000-00-00', '', '中国-辽宁');
INSERT INTO `users` VALUES ('2', '123', '123', '0', '0000-00-00', '', 'zs');
INSERT INTO `users` VALUES ('3', '1', '', '127', '0000-00-00', '20160907093745939.jpg', 'eryghsrt');
INSERT INTO `users` VALUES ('4', '2', '', '127', '0000-00-00', '20160907093843929.jpg', '34534t');
INSERT INTO `users` VALUES ('5', 'asrf24', '', '127', '0000-00-00', '20160907093952116.jpg', 'dfsgdfg');
INSERT INTO `users` VALUES ('14', '高', '123', '0', '2016-09-10', '20160907120543560.', '啊对方感受到');
INSERT INTO `users` VALUES ('15', '包', '123', '0', '2016-09-17', '20160907120816472.', '对方性格和');
