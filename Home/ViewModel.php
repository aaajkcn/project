<html>
<head>
	<meta charset="UTF-8">
	<title>ViewModel</title>
</head>
<body>
	<?php 
		class ViewModel{
			private $data=array();//记录从Model层读来的数据
			private $render;//记录视图层文件名 user/listusers.php
		
			function __construct($template){
				//构建一个完整的路径名
				$file='Home/view/'.strtolower($template).'.php';//视图层完整路径名
				if (file_exists($file)) {
					$this->render=$file;

				}

			}
			function assign($variable,$value){
				//把从model层获取的数据赋给外面的数组$data['users']
				$this->data[$variable]=$value;


			}

			function display(){
				//准备数据，然后包含视图层文件显示
				$data=$this->data;
				include "$this->render";
			}

		} 
		
	?>
</body>
</html>