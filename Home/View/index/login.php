<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>用户登录界面</title>
  <link rel="stylesheet" href="./assets/css/bootstrap.min.css">
</head>
<body>
  <div class="container-fluid">
    <!--nav开始-->
    <ul class="nav nav-tabs">
      <li role="presentation" class="active"><a href="index.php?c=index&m=index">首页</a></li>
      <li role="presentation"><a href="index.php?c=index&m=listall&tid=1">男装</a></li>
      <li role="presentation"><a href="index.php?c=index&m=listall&tid=2">女装</a></li>
      <li role="presentation"><a href="index.php?c=index&m=listall&tid=3">数码</a></li>
      <li role="presentation"><a href="index.php?c=index&m=listall&tid=4">运动</a></li>
      <li role="presentation"><a href="index.php?c=index&m=listall&tid=5">生活</a></li>
      <li role="presentation"><a href="index.php?c=index&m=listall&tid=6">其它</a></li>
      <formaction="search.php"class="navbar-form navbar-left" role="search" >
      <div class="form-group">
        <inputplaceholder="Search">
      </div>
      <button type="submit" class="btn btn-default">搜索</button>
    </form>
    <ul class="nav navbar-nav navbar-right">
      <?php session_start(); 
      if(isset($_SESSION["uname"])){ 
        ?>
        <li class="active"><a href="#" >欢迎
         <?php echo " ".$_SESSION["uname"]." ";
       } else {
        ?> 
        <li role="presentation"><a href="index.php?c=index&m=login">登录</a></li>
        <li role="presentation"><a href="index.php?c=index&m=reguser">注册</a></li>
        <li class="active"><a href="#" >欢迎<?php echo "您";}?>访问本站</a></li>
        <li role="presentation"><a href="index.php?c=index&m=logout">注销</a></li>
        <li role="presentation"><a href="index.php?c=index&m=reguser">个人中心</a></li>
      </ul>
    </div>
    <div class="container">
      <div class="row">
       <br>
     </div>
     <?php session_start(); ?>
     <form action="index.php?c=index&m=dologin" method="post" class="form-horizontal col-lg-6 col-md-offset-3  bg-success" role="form">
      <h2 class="text-center"><strong>用户登录</strong><small>请登录</small></h2>
      <div class="form-group">
        <label for="name" class="col-sm-2 control-label">用户名</label>
        <div class="col-sm-10">
          <input type="text" name="uname" id="name" class="form-control" placeholder="请输入用户名">
        </div>
      </div>
      <div class="form-group">
        <label for="password" class="col-sm-2 control-label">密码</label>
        <div class="col-sm-10">
          <input type="password" name="password" id="password" class="form-control" placeholder="请输入密码">
          <span class="help-block">请注意账号安全</span>
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <div class="checkbox">
            <label>
              <input type="checkbox">记住密码</label>
            </div>
          </div>
        </div>
    <!-- <div class="col-sm-offset-5 col-sm-8">
                    <input type="submit" class="btn btn-default" value="提交">
                    <input type="reset" class="btn btn-default" value="重置">
                  </div> -->
                  <button type="submit" class="btn btn-warning btn-lg btn-block" data-toggle="moda2" data-target="#myModa2">登录</button>
                  <button type="reset" class="btn btn-default btn-lg btn-block" data-toggle="moda2" data-target="#myModa2">重置</button>
                </form>
              </div>
            </body>
            </html>