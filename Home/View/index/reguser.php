<html>
<head>
	<meta charset="UTF-8">
	<title>用户注册</title>
	<link rel="stylesheet" href="./assets/css/bootstrap.min.css">
</head>
<body>
  <div class="container-fluid">
    <!--nav开始-->
    <ul class="nav nav-tabs">
      <li role="presentation" class="active"><a href="index.php?c=index&m=index">首页</a></li>
      <li role="presentation"><a href="index.php?c=index&m=listall&tid=1">男装</a></li>
      <li role="presentation"><a href="index.php?c=index&m=listall&tid=2">女装</a></li>
      <li role="presentation"><a href="index.php?c=index&m=listall&tid=3">数码</a></li>
      <li role="presentation"><a href="index.php?c=index&m=listall&tid=4">运动</a></li>
      <li role="presentation"><a href="index.php?c=index&m=listall&tid=5">生活</a></li>
      <li role="presentation"><a href="index.php?c=index&m=listall&tid=6">其它</a></li>
      <form
      action="search.php"class="navbar-form navbar-left" role="search" >
      <div class="form-group">
        <input
        placeholder="Search">
      </div>
      <button type="submit" class="btn btn-default">搜索</button>
    </form>
    <ul class="nav navbar-nav navbar-right">
    <li class="active"><a href="">欢迎<?php
        session_start();
        if(isset($_SESSION["uname"]))
            echo " ".$_SESSION["uname"]." ";
        else{
            echo "您";
        }
        ?>访问本网站</a>
    </li>
    <?php
//登录成功才显示
    if(isset($_SESSION["uname"]))
    {
        ?>
        <li><a href="index.php?c=index&m=logout">注销</a></li>
        <li><a href="index.php?c=index&m=reguser">个人中心</a></li>
        <?php
    }else{
        ?>

        <li role="presentation"><a href="index.php?c=index&m=login">登录</a></li>
         <li role="presentation"><a href="index.php?c=index&m=reguser">注册</a></li>
        <?php
    }
    ?>
</ul>
    </div>
    <br>；
    <br>；
    <br>；

    <form action="index.php?c=index&m=adduser" method="post" class="form-horizontal col-lg-6 col-md-offset-3  bg-success" role="form">
      <h2 class="text-center"><strong>用户注册</strong><small>请填写</small></h2>
      <div class="form-group">
        <label for="inputname" class="col-md-3 control-label col-md-offset-1">用户名</label>
        <div class="col-md-5">
          <input type="text" name="uname" class="form-control" id="inputname" placeholder="用户名">
        </div>
      </div>

      <div class="form-group">
        <label for="inputpass" class="col-md-3 control-label col-md-offset-1">密码</label>
        <div class="col-md-5">
          <input type="password" name="password" class="form-control" id="inputpass" placeholder="密码">
        </div>
      </div>

      <div class="form-group">
        <label for="inlineRadio" class="col-md-3 control-label col-md-offset-1">性别</label>
        <div class="col-md-5">
         <label class="radio-inline">
          <input type="radio" name="gender" id="inlineRadio1" value="0"> 男
        </label>
        <label class="radio-inline">
          <input type="radio" name="gender" id="inlineRadio2" value="1"> 女
        </label>
      </div>

    </div>
    
    <div class="form-group">
      <label for="inputdate" class="col-md-3 control-label col-md-offset-1">生日</label>
      <div class="col-md-5">
        <input type="date" class="form-control" id="inputdate" name="birthdate">
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-3 control-label col-md-offset-1">头像</label>
      <div class="col-md-5">
       <input type="file" name="pic">	
     </div>
   </div>

    <div class="form-group">
      <label for="inputdate" class="col-md-3 control-label col-md-offset-1">地址</label>
      <div class="col-md-5">
        <input type="text" class="form-control" id="inputdate" name="adress">
      </div>
    </div>

   <div class="form-group">
    <div class="col-sm-offset-5 col-sm-6 "> <!-- 向右位移两个方格 -->
      <button type="submit" class="btn btn-default">注册</button>
      <button type="reset" class="btn btn-default">重置</button>
    </div>
  </div>



</form>
</body>
 </html>
 