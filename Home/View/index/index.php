<!DOCTYPE HTML>
<html>
<head>
<title>Home</title>
<link href="Home/View/index/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary JavaScript plugins) -->
<script type='text/javascript' src="Home/View/index/js/jquery-1.11.1.min.js"></script>
<!-- Custom Theme files -->
<link href="Home/View/index/css/style.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- start menu -->
<link href="Home/View/index/css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="Home/View/index/js/megamenu.js"></script>
<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
<script src="Home/View/index/js/menu_jquery.js"></script>
<script src="Home/View/index/js/simpleCart.min.js"> </script>
</head>
<body>
<!-- header_top -->
<div class="top_bg">
	<div class="container">
		<div class="header_top">
			<div class="top_right">
				<ul>
					<li><a href="#">帮助</a></li>|
					<li><a href="contact.html">联系客服</a></li>|
					<li><a href="#">我的消息</a></li>
				</ul>
			</div>
			<div class="top_left">
				<h2><span></span>请拨打 : xxx xxxx xxx</h2>
			</div>
				<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!-- header -->
<div class="header_bg">
<div class="container">
	<div class="header">
	<div class="head-t">
		<div class="logo">
			<a href="index.html"><img src="Home/View/index/images/logo.png" class="img-responsive" alt=""/> </a>
		</div>
		<!-- start header_right -->
		<div class="header_right">
			<div class="rgt-bottom">
			
 <?php
//登录成功才显示
    if(isset($_SESSION["uname"]))
    {
        ?>
        <div class="rgt-bottom">
        <div id="loginContainer"><a href="index.php?c=index&m=logout">注销</a></div>
        <div id="loginContainer"><a href="index.php?c=index&m=addgoods&uname=<?php echo $_SESSION['uname']; ?>">个人中心</a></div>
        </div>
        <?php
    }else{
        ?>
        <div id="loginContainer"><a href="index.php?c=index&m=login">登录</a></div>
         <div id="loginContainer"><a href="index.php?c=index&m=reguser">注册</a></div>
        <?php
    }
    ?>
      
		<div class="search">
		    <form>
		    	<input type="text" value="" placeholder="search...">
				<input type="submit" value="">
			</form>
		</div>
		<div class="clearfix"> </div>
		</div>
		<div class="clearfix"> </div>
	</div>
		<!-- start header menu -->
		<ul class="megamenu skyblue">
			<li><a class="color2" href="index.php?c=index&m=index">首页</a></li>				
			<li><a class="color5" href="index.php?c=index&m=listall&tid=1">男装</a></li>
			<li><a class="color6" href="index.php?c=index&m=listall&tid=2">女装</a></li>				
            <li><a class="color7" href="index.php?c=index&m=listall&tid=3">数码</a></li>					
			<li><a class="color8" href="index.php?c=index&m=listall&tid=4">运动</a></li>
			<li><a class="color9" href="index.php?c=index&m=listall&tid=5">生活</a></li>
			<li><a class="color9" href="index.php?c=index&m=listall&tid=6">其它</a></li>
		 </ul> 
	</div>
</div>
</div>
<div class="arriv">
	<div class="container">
		<div class="arriv-top">
			<div class="col-md-6 arriv-left">
				<img src="photos/20160908075918826.jpg" class="img-responsive" alt="">
				<div class="arriv-info">
					<div class="crt-btn">

						<a href="index.php?c=index&m=content&gid=27">看一看</a>
					</div>
				</div>
			</div>
			<div class="col-md-6 arriv-right">
				<img src="photos/20160908080010172.jpg" class="img-responsive" alt="">
				<div class="arriv-info">
					<div class="crt-btn">

						<a href="index.php?c=index&m=content&gid=28">去购买</a>
					</div>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
		<div class="arriv-bottm">
			<div class="col-md-8 arriv-left1">
				<img src="photos/20160908080049277.jpg" class="img-responsive" alt="">
				<div class="arriv-info1">
					<div class="crt-btn">
          <h3></h3>
						<a href="index.php?c=index&m=content&gid=29">去购买</a>
					</div>
				</div>
			</div>
			<div class="col-md-4 arriv-right1">
				<img src="photos/20160908080128809.jpg" class="img-responsive" alt="">
				<div class="arriv-info2">
					<a href="index.php?c=index&m=content&gid=30"><h3>登山鞋<i class="ars"></i></h3></a>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
		<div class="arriv-las">
			<div class="col-md-4 arriv-left2">
				<img src="photos/20160908080218396.jpg" class="img-responsive" alt="">
				<div class="arriv-info2">
					<a href="index.php?c=index&m=content&gid=31"><h3>眼镜<i class="ars"></i></h3></a>
				</div>
			</div>
			<div class="col-md-4 arriv-middle">
				<img src="photos/20160908080322394.jpg" class="img-responsive" alt="">
				<div class="arriv-info3">
					<div class="crt-btn">
						<a href="index.php?c=index&m=content&gid=32">去购买</a>
					</div>
				</div>
			</div>
			<div class="col-md-4 arriv-right2">
				<img src="photos/20160908080405742.jpg" class="img-responsive" alt="">
				<div class="arriv-info2">
					<a href="index.php?c=index&m=content&gid=33"><h3>手表<i class="ars"></i></h3></a>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<div class="special">
	<div class="container">
		<h3>推荐</h3>
		<div class="specia-top">
			<ul class="grid_2">
		<li>
				<a href="index.php?c=index&m=content&gid=37"><img src="photos/20160908080715907.jpg" class="img-responsive" alt=""></a>
				<div class="special-info grid_1 simpleCart_shelfItem">
					<div class="item_add"><span class="item_price"><h6>￥40.00</h6></span></div>
					<div class="item_add"><span class="item_price"><a href="#">加入购物车</a></span></div>
				</div>
		</li>
		<li>
				<a href="index.php?c=index&m=content&gid=34"><img src="photos/20160908080508630.jpg" class="img-responsive" alt=""></a>
				<div class="special-info grid_1 simpleCart_shelfItem">
					<div class="item_add"><span class="item_price"><h6>￥60.00</h6></span></div>
					<div class="item_add"><span class="item_price"><a href="#">加入购物车</a></span></div>
			</div>
		</li>
		<li>
				<a href="index.php?c=index&m=content&gid=35"><img src="photos/20160908080556639.jpg" class="img-responsive" alt=""></a>
				<div class="special-info grid_1 simpleCart_shelfItem">
					<div class="item_add"><span class="item_price"><h6>￥14.00</h6></span></div>
					<div class="item_add"><span class="item_price"><a href="#">加入购物车</a></span></div>
			</div>
		</li>
		<li>
				<a href="index.php?c=index&m=content&gid=36"><img src="photos/20160908080636152.jpg" class="img-responsive" alt=""></a>
				<div class="special-info grid_1 simpleCart_shelfItem">
					<div class="item_add"><span class="item_price"><h6>￥37.00</h6></span></div>
					<div class="item_add"><span class="item_price"><a href="#">加入购物车</a></span></div>
				</div>
		</li>
		<div class="clearfix"> </div>
	</ul>
		</div>
	</div>
</div>


</body>
</html>