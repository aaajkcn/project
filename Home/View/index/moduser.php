<?php 
require_once('common1.php'); 
?>

<div class="inner" style="min-height:600px; width:1000px;">
  <h2>修改个人信息</h2>

  <form method="post"  action="index.php?c=index&m=update&uname=<?php echo $_SESSION['uname']; ?>"  enctype="multipart/form-data">


    <div class="row">
      <label for="exampleInputUserName1" class="col-md-1 control-label col-md-offset-4">姓名：</label>
      <div class="col-md-3">
        <input type="text" name="uname" class="form-control"  value=<?php  echo $data['uid']['uname'];  ?>>
      </div>
    </div>

    <div class="row">
    <label for="exampleInputUserName1" class="col-md-1 control-label col-md-offset-4">性别:</label>
      <div class="col-md-3">
        <input type="radio" name="gender" value="0" <?php  if(!$data['uid']['gender']) echo 'checked';  ?>>男
        <input type="radio" name="gender" value="1" <?php  if($data['uid']['gender']) echo 'checked';  ?>>女
      </div>
    </div>

    <div class="row">
      <label for="exampleInputUserName1" class="col-md-1 control-label col-md-offset-4">生日：</label>
      <div class="col-md-3">
        <input type="date" name="birthdate"  class="form-control" value=<?php  echo $data['birthdate'];  ?>>
      </div>
    </div>


      <div class="row">
        <label for="exampleInputUserName1" class="col-md-1 control-label col-md-offset-4">头像</label>
        <div class="col-md-6">
         <img src='<?php  echo "assets/images/".$data['uid']['pic'];  ?>' width=100 height=150>
         <input type="file" name="pic">    </div>
       </div>


     <div class="row">
      <label for="exampleInputUserName1" class="col-md-1 control-label col-md-offset-4">地址</label>
      <div class="col-md-3">
        <textarea name="intro" cols="40" rows="2"><?php  echo $data['uid']['intro'];?></textarea>    
      </div>
      </div>

       <div class="row">
        <div class="col-sm-offset-5 col-sm-5">
          <input type="submit" class="btn btn-default" value="更新">
          <input type="reset" class="btn btn-default" value="重置">
        </div>
      </div>

<!-- 
 <tr>
    <td>姓名：</td>
    <td><input type="text" name="uname" size=30  value=<?php  echo $data['uid']['uname'];  ?>></td>
  </tr>  <tr>
    <td>性别:</td>
    <td>
      <input type="radio" name="gender" value="0" <?php  if(!$data['uid']['gender']) echo 'checked';  ?>>男
      <input type="radio" name="gender" value="1" <?php  if($data['uid']['gender']) echo 'checked';  ?>>女</td>
    </tr>  <tr>
      <td>生日：</td>
      <td><input type="date" name="birthdate"  size=30 value=<?php  echo $data['birthdate'];  ?>></td>
    </tr>  -->
   <!--  <tr>
      <td >爱好：</td>
      <td>
       <input type="checkbox" name="hobby[]" value="旅游" <?php  if(strstr($data['uid']['hobby'],'旅游')) echo 'checked';  ?>>旅游
       <input type="checkbox" name="hobby[]" value="登山" <?php  if(strstr($data['uid']['hobby'],'登山')) echo 'checked';  ?>>登山
       <input type="checkbox" name="hobby[]" value="健身" <?php  if(strstr($data['uid']['hobby'],'健身')) echo 'checked';  ?>>健身
       <input type="checkbox" name="hobby[]" value="上网" <?php  if(strstr($data['uid']['hobby'],'上网')) echo 'checked';  ?>>上网
       <input type="checkbox" name="hobby[]" value="游泳" <?php  if(strstr($data['uid']['hobby'],'游泳')) echo 'checked';  ?>>游泳
     </td>
   </tr>
   <tr>
    <td width=150>学历:</td>
    <td>
      <select name="degree">
       <option value="0">---请选择---</option>
       <option value="1" <?php  if($data['uid']['degree']=='1') echo 'selected';  ?>>高中</option>
       <option value="2" <?php  if($data['uid']['degree']=='2') echo 'selected';  ?>>大学</option>
       <option value="3" <?php  if($data['uid']['degree']=='3') echo 'selected';  ?>>研究生</option>
       <option value="4" <?php  if($data['uid']['degree']=='4') echo 'selected';  ?>>博士生</option>
     </select>
   </td>
 </tr>
 <tr>
  <td width=150>自我介绍：</td>
  <td>
    <textarea name="intro" cols="40" rows="5"><?php  echo $data['uid']['intro'];  ?></textarea>
  </td>
</tr>
<tr>
  <td width=150>头像</td>   
  <td>
    <img src='<?php  echo "assets/images/".$data['uid']['pic'];  ?>' width=100 height=150>
    <input type="file" name="pic">
  </td>
</tr>
<tr>
  <td width=150></td>
  <td>
    <input type="submit" value="更新">
    <input type="reset" value="重置">
  </td>
</tr> -->

</form>
</div>


<?php 
require_once('common2.php');
?>
