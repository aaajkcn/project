<?php 
require_once('common1.php'); 
?>

<div class="inner" style="min-height:600px; width:1000px;">
  <h2>修改商品信息</h2>

  <form method="post"  action="index.php?c=index&m=updategood&uname=<?php echo $_SESSION['uname']; ?>"  enctype="multipart/form-data">


 <!--    <div class="form-group">
      <label for="exampleInputUserName1" class="col-md-1 control-label col-md-offset-7">姓名：</label>
      <div class="col-md-4">
        <input type="text" name="uname" size=30  value=<?php  echo $data['uid']['uname'];  ?>>
      </div>
    </div>
 -->
      <div class="row">
        <label for="exampleInputUserName1" class="col-md-1 control-label col-md-offset-4">商品名称</label>
        <div class="col-md-4">
          <input type="text" name="goodname" class="form-control"  value=<?php  echo $data['gid']['goodname'];  ?>>
        </div>
      </div>

      <div class="row">
        <label for="exampleInputAge1" class="col-md-1 control-label col-md-offset-4">价格</label>
        <div class="col-md-4">
          <input type="number" name="price" class="form-control"  value=<?php  echo $data['gid']['price'];  ?>>
        </div>
      </div>
       <div class="row">
        <label for="exampleInputAge1" class="col-md-1 control-label col-md-offset-4">手机号码</label>
        <div class="col-md-4">
           <input type="int" name="phone" class="form-control"  value=<?php  echo $data['gid']['phone'];  ?>>
        </div>
      </div>


      <div class="row">
        <label class="col-md-1 control-label col-md-offset-4">商品类型</label>
        <div class="col-md-4">
          <select class="form-control" name="tid">
            <option value="">请选择</option>
            <option value=1 <?php  if($data['gid']['tid']=='1') echo 'selected';  ?>>男装</option>
            <option value=2 <?php  if($data['gid']['tid']=='2') echo 'selected';  ?>>女装</option>
            <option value=3 <?php  if($data['gid']['tid']=='3') echo 'selected';  ?>>数码</option>
            <option value=4 <?php  if($data['gid']['tid']=='4') echo 'selected';  ?>>运动</option>
            <option value=5 <?php  if($data['gid']['tid']=='5') echo 'selected';  ?>>生活</option>
            <option value=6 <?php  if($data['gid']['tid']=='6') echo 'selected';  ?>>其它</option>
          </select>
        </div>
      </div>



      <div class="row">
        <label for="exampleInputFile" class="col-md-1 control-label col-md-offset-4">商品图片</label>
        <div class="col-md-4">
         
          <img src="photos/<?php echo $data['gid']['pic'];?>" width=100 height=150>
         <input type="file" name="pic">
        </div>
      </div>

           <div class="row">
        <label for="exampleInputTextarea1" class="col-md-1 control-label col-md-offset-4">商品介绍</label>
        <div class="col-md-4">
          <textarea name="intro" cols="40" rows="2"><?php  echo $data['gid']['intro'];?></textarea>
        </div>
      </div>
       <div class="row">
        <div class="col-sm-offset-5 col-sm-5">
          <input type="submit" class="btn btn-default" value="发布">
          <input type="reset" class="btn btn-default" value="重置">
        </div>
      </div>

</form>
</div>


<?php 
require_once('common2.php');
?>
