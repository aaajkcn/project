<!DOCTYPE HTML>
<html>
<head>
<title>Home</title>
<link href="Home/View/index/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary JavaScript plugins) -->
<script type='text/javascript' src="Home/View/index/js/jquery-1.11.1.min.js"></script>
<!-- Custom Theme files -->
<link href="Home/View/index/css/style.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- start menu -->
<link href="Home/View/index/css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="Home/View/index/js/megamenu.js"></script>
<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
<script src="Home/View/index/js/menu_jquery.js"></script>
<script src="Home/View/index/js/simpleCart.min.js"> </script>
</head>
<body>
<!-- header_top -->
<div class="top_bg">
  <div class="container">
    <div class="header_top">
      <div class="top_right">
        <ul>
          <li><a href="#">帮助</a></li>|
          <li><a href="contact.html">联系客服</a></li>|
          <li><a href="#">我的消息</a></li>
        </ul>
      </div>
      <div class="top_left">
        <h2><span></span>请拨打 : xxx xxxx xxx</h2>
      </div>
        <div class="clearfix"> </div>
    </div>
  </div>
</div>
<!-- header -->
<div class="header_bg">
<div class="container">
  <div class="header">
  <div class="head-t">
    <div class="logo">
      <a href="index.html"><img src="Home/View/index/images/logo.png" class="img-responsive" alt=""/> </a>
    </div>
    <!-- start header_right -->
    <div class="header_right">
      <div class="rgt-bottom">
      
 <?php
//登录成功才显示
    if(isset($_SESSION["uname"]))
    {
        ?>
        <div class="rgt-bottom">
        <div id="loginContainer"><a href="index.php?c=index&m=logout">注销</a></div>
        <div id="loginContainer"><a href="index.php?c=index&m=modgoods&uname=<?php echo $_SESSION['uname']; ?>">个人中心</a></div>
        </div>
        <?php
    }else{
        ?>
        <div id="loginContainer"><a href="index.php?c=index&m=login">登录</a></div>
         <div id="loginContainer"><a href="index.php?c=index&m=reguser">注册</a></div>
        <?php
    }
    ?>
      
    <div class="search">
        <form>
          <input type="text" value="" placeholder="search...">
        <input type="submit" value="">
      </form>
    </div>
    <div class="clearfix"> </div>
    </div>
    <div class="clearfix"> </div>
  </div>
    <!-- start header menu -->
    <ul class="megamenu skyblue">
      <li><a class="color2" href="index.php?c=index&m=index">首页</a></li>        
      <li><a class="color5" href="index.php?c=index&m=listall&tid=1">男装</a></li>
      <li><a class="color6" href="index.php?c=index&m=listall&tid=2">女装</a></li>        
            <li><a class="color7" href="index.php?c=index&m=listall&tid=3">数码</a></li>          
      <li><a class="color8" href="index.php?c=index&m=listall&tid=4">运动</a></li>
      <li><a class="color9" href="index.php?c=index&m=listall&tid=5">生活</a></li>
      <li><a class="color9" href="index.php?c=index&m=listall&tid=6">其它</a></li>
     </ul> 
  </div>
</div>
</div>
<!-- content -->
<div class="container ">
<div class="women_main">
	<!-- start content -->
			<div class="row single">
				<div class="col-md-12 det">
				  <div class="single_left">
					<div class="grid images_3_of_2">
						<ul id="etalage">
							<li>
								<a href="optionallink.html">
									<img class="etalage_thumb_image" src="photos/<?php echo $data['content']['pic'];?>" class="img-responsive" />
									
								</a>
							</li>
							
						</ul>
						 <div class="clearfix"></div>		
				  </div>
				  <div class="desc1 span_2.5_of_2">
					
                 
    <table class="table table-hover">

      <tr>
        <td>商品名称</td>
        <td><?php echo $data['content']['goodname'];?></td>
      </tr>
      <tr>
        <td>商品类型</td>
        <td><?php 
          switch ($data['content']['tid']) {
            case '1':
            echo "男装";
            break;
            case '2':
            echo "女装";
            break;
            case '3':
            echo "数码";
            break;
            case '4':
            echo "运动";
            break;
            case '5':
            echo "生活";
            break;
            default:
            echo "其它";
            break;
          }
          ?></td>
        </tr>

        <tr>
          <td>价格</td>
          <td><?php echo $data['content']['price'];?></td>
        </tr>



      </table>
 
					<div class="btn_form">
						<a href="#">购买</a>
						<a href="#">加入购物车</a>
					</div>
					
					
			   	 </div>
          	    <div class="clearfix"></div>
          	   </div>
          	    <div class="single-bottom1">
					<h6>商品介绍</h6>
        <?php echo $data['content']['intro'];?>
				</div>

	       </div>	

		   <div class="clearfix"></div>		
	  </div>
	<!-- end content -->
</div>
</div>
<div class="foot-top">

</div>

</body>
</html>