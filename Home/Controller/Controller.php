<?php 
require 'Home/ViewModel.php';
class Controller{
	protected $view;
	function __construct(){
		session_start();
		$this->view=new ViewModel($_GET['c'].'/'.$_GET['m']);

	}
	function assign($variable,$value){
		$this->view->assign($variable,$value);
	}
	function display(){
		$this->view->display();
	}
}
?>