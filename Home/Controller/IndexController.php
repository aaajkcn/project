<?php 
require 'Home/Model/IndexModel.php';
require 'Controller.php';
class IndexController  extends Controller{
	function __construct(){
		parent::__construct();
	}
	function index(){
		$model=new IndexModel();
		$data=$model->getList();
			//return $data;

		$this->assign('goods',$data);
			$this->display();//送到view层显示数据
		}
		function listall(){
			$tid=$_GET['tid'];
			$model=new IndexModel();
			$data=$model->getListall($tid);
			//return $data;

			$this->assign('listall',$data);
			$this->display();//送到view层显示数据
		}

		function good0(){
			$uname=$_GET["uname"];
			$sign=$_GET['sign'];
			$model=new IndexModel();
			$data=$model->getGood($sign,$uname);
			//return $data;

			$this->assign('list',$data);
			$this->display();//送到view层显示数据
		}


		function good1(){
			$uname=$_GET["uname"];
			$sign=$_GET['sign'];
			$model=new IndexModel();
			$data=$model->getGood($sign,$uname);
			//return $data;

			$this->assign('list',$data);
			$this->display();//送到view层显示数据
		}



		function content(){
			$gid=$_GET['gid'];
			$model=new IndexModel();
			$data=$model->getContent($gid);
			$this->assign('content',$data);
			$this->display();//送到view层显示数据
		}
		function reguser(){
			$this->display();
		}
		function adduser(){
			$data=$_POST;
			$file=$_FILES;
			$model=new IndexModel();
			$result=$model->addUser($data,$file);
			if ($result) {
				echo "注册成功，3秒后返回首页";
				header("refresh:3;url='index.php?c=index&m=index'");
			}else{
				echo "注册失败，3秒后返回注册页面重新注册";
				header("refresh:3;url='index.php?c=index&m=reguser'");
			}

		}
		function login(){
			$this->display();
		}
		function dologin(){
			$uname=$_POST["uname"];
			$password=$_POST["password"];
			$model=new IndexModel();
			$result=$model->dologin($uname,$password);
			if ($result) {
				echo "欢迎您".$uname;
				$_SESSION["uname"]=$uname;
				$_SESSION["password"]=$password;
				header("refresh:3;url='index.php?c=index&m=index'");
			} else {
				echo "请重试，3秒后返回首页重新登陆";
			}
		}
	/*	
		function search(){
			$data=$_POST;
			$model=new IndexModel();
			$result=$model->search($data);
			if ($result) {
				$row=$result;
				$vid=$row['vid'];
				header("refresh:3;url='index.php?c=index&m=searchlist&vid=$vid'");
    			} else
       			 echo "没有搜索到该内容，3秒后返回";
   

       			}*/
       			function searchlist(){
       				$this->display();
       				$tid=$_GET['tid'];
       				$model=new IndexModel();
       				$data=$model->getList($tid);
			//return $data;

       				$this->assign('listall',$data);
       				$this->display();
       			}
       			function logout(){
       				session_start();
       				session_destroy();
       				echo "注销成功";
       				header("refresh:2;url='index.php?c=index&m=index'");


       			}
                function modgoods(){
                	$gid=$_GET['gid'];
                	$model = new IndexModel();
       				$data = $model->getContent($gid);
       				$this->assign('gid',$data);
                	$this->display();
                }


       			function moduser(){
       				$uname = $_GET['uname'];
       				$model = new IndexModel();
       				$data = $model->getUserByID($uname);
		// var_dump($data);
       				$this->assign('uid',$data);
       				$this->display();

       			}

       			function update(){
       				$data = $_POST;
       				$model = new IndexModel();
       				$result = $model->updateUser($data);
       				var_dump($result);
       				if($result){
       					echo '更新成功 <a href="index.php?c=index&m=index">如果没有跳转，请点这里跳转</a>';
       					header("url='index.php?c=index&m=index'");
       				}
       				else{
       					echo "更新失败！<br/>";
       					echo "<a href='index.php?c=index&m=index'>返回</a>";
       				}
       			}
       			function changepassword(){
		// $uname = $_GET['uname'];
		// $model = new IndexModel();
		// $data = $model->getUserByID($uname);
		// $this->assign('user',$data);
       				$this->display();

       			}

       			function dochangepassword(){
       				$d=$_POST;
       				$password2=$d['password2'];
       				$password1=$d['password1'];
       				$model = new IndexModel();
       				if($password1===$password2){
       					$result = $model->getPassword($d);
       					if($result!=1){
       						echo "修改失败！<br/>";
       						echo "<a href='index.php?c=index&m=index'>返回</a>";
       					}else{
       						echo '修改成功！';
       						header("refresh:3;url='index.php?c=index&m=index'");
       					}

       				}else{
       					echo "请确认密码一样！<br/>";
       					echo "<a href='index.php?c=index&m=index'>返回</a>";
       				}
       			}


       			function userinfo(){
    	   	// var_dump($_SESSION);
    	   	// die;
       				$name = $_SESSION['uname'];
   //  	   	$sql =
			// $model=new IndexModel();
       				$data=$model->getUserByName($name);
       				$id = $data['uid'];
       				$this->assign('id',$id);
			$this->display();//送到view层显示数据

		}



		function addgoods(){
			$this->display();
		}
		function doaddgoods(){
			
			$data=$_POST;
			$file=$_FILES;
			$model=new IndexModel();
			$result=$model->addGood($data,$file);
			if ($result) {
				echo "添加成功，3秒后返回首页";
				header("refresh:3;url='index.php?c=index&m=index'");
			}else{
				echo "添加失败，3秒后返回首页";
				header("refresh:3;url='index.php?c=index&m=index'");
			}

		}


		function del(){
			$gid = $_GET['gid'];
			$model = new IndexModel();
			$result = $model->del($gid);
			if($result){
				echo '删除成功 <a href="index.php?c=index&m=index">如果没有跳转，请点这里跳转</a>';
				header("refresh:3;url='index.php?c=index&m=index'");
			}
			else{
				echo "删除失败！<br/>";
				echo "<a href='index.php?c=index&m=index'>返回</a>";
			}
		}
	}

	?>