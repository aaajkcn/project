<?php 
require 'Common/ConnDB.php';
require 'Common/config.php';
class Model{
	protected $conn;
	/*1.private 是完全私有的,只有当前类中的成员能访问到.
	  2.protected 是受保护的,只有当前类的成员与继承该类的类才能访问.*/
	function __Construct(){
		$this->conn = new ConnDB(DB_TYPE,DB_HOST,DB_USER,DB_PWD,DB_DBNAME,DB_CHARSET);//传参

	}
	function uploadfile($file){
		 $arr=explode('.', $file['pic']['name']);
       	 $suffix=$arr[count($arr)-1];
       	 $newname=date("YmdHis").rand(100,999).'.'.$suffix;
         $filepath = './photos/';
       	 move_uploaded_file($_FILES['pic']['tmp_name'], $filepath.$newname);//上传文件到指定文件夹，tmp_name是file里带的
       	 return $newname;
	}
 
 }
 ?>