#project

1.项目名称：东软闲置

2.项目简介：前台：普通用户登录、注册、商品查询浏览及商品分类浏览、商品详细信息浏览、个人货品发布修改、我发布的商品浏览及已买到的商品浏览、修改密码和个人资料等；
            后台：管理员登录、用户管理和货品管理等功能页。 

3.项目分工：  组长： 高保财：前台框架搭构、前台model层和controller层及协助view层，画前台uml图及数据库的优化。

              组员： 钱地红：前台view层，画前台uml图及收集数据库所需数据。

              组员： 李红飞：后台界面框架，管理员对商品的管理模块及对应的uml图，文档编写。

              组员： 李帅：后台管理员对用户的管理模块方面及用户界面的uml图，文档编写及数据库设计。

4.项目说明：（1）.首页地址：http://localhost/market/index.php?c=index&m=index
            （2）.后台地址：http://localhost/mvc-umis/admin/login.php
            （3）.用户登录：用户名：用户甲
                            密码：1234
            （4）.后台登录：用户名：li
                            密码：123
            （5）.可以查询我发布的商品和我买到的商品。
                  我发布的商品：http://localhost/market/index.php?c=index&m=good0&uname=用户甲&sign=0
                  我买到的商品：http://localhost/market/index.php?c=index&m=good1&uname=用户甲&sign=1