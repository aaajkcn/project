﻿<?php  
// 连接数据库的类  
class ConnDB  
{  
    var $dbtype;  
    var $host;  
    var $user;  
    var $pwd;  
    var $dbname;  
    var $conn;  
    // 构造方法  
    function ConnDB($dbtype,$host,$user,$pwd,$dbname)  
    {  
            $this->dbtype = $dbtype;  
            $this->host = $host;  
            $this->pwd = $pwd;  
            $this->dbname = $dbname;  
            $this->user = $user;  
            $this->conn = @mysql_connect($this->host,$this->user,$this->pwd) or die("数据库服务器连接错误".mysql_error());   
            @mysql_select_db($this->dbname,$this->conn) or die("数据库访问错误".mysql_error());  
            @mysql_query("set names utf8");// 设置编码格式 
    }  
    
    function query($sql)  
    {  
        $sqltype = strtolower(substr(trim($sql),0,6));// 截取sql语句中的前6个字符串,并转换成小写  
        $result = mysql_query($sql,$this->conn);// 执行sql语句 
        // $result = mysql_query($sql,$this->conn) or die("查询失败！".mysql_error()); 
        $calback_arrary = array();// 定义二维数组  
        if ("select" == $sqltype)// 判断执行的是select语句  
        {  
              
            if (false == $result)  
            {  
                return false;     
            }  
            else if (0 == mysql_num_rows($result))  
            {  
                return false;  
            } 
            else if(1 == mysql_num_rows($result)){
                return mysql_fetch_assoc($result);
            }
            else  
            {  
                while($result_array = mysql_fetch_assoc($result))  
                {  
                array_push($calback_arrary, $result_array);  
                }  
                return $calback_arrary;// 成功返回查询结果的数组     
            }  
        }  
        else if ("update" == $sqltype || "insert" == $sqltype || "delete" == $sqltype)  
        {  
                if ($result)  
                {  
                    return true;  
                }  
                else  
                {  
                    return false;  
                }  
        }  
    }       
    function __destruct()  
    {  
        $this->CloseDB();      
    }  
    function CloseDB()  
    {  
        mysql_close($this->conn);  
    }  
          
}  
  
?>  